const express = require('express');
const router = express.Router();
const nodeMailer = require('nodemailer')
const RandomCodeGenerator = require("randomatic");
const User = require('../models/User')
const bcript = require('bcryptjs');
const jwt = require('jsonwebtoken'); 
// add methods here 
//Registration
const transporter =  nodeMailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.email,
      pass: process.env.password
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false,
    }
  });
router.post('/newPassword/:code',async (req,res)=>{
    const code = req.params.code
    const { email,password,passwordConfirm } = req.body
    const foundUser = await User.findOne({forgetPasswordCode:code,email:email})
    if(!foundUser)
        return res.status(400).send({success:false,message:"user not found"})
    if(password != passwordConfirm)
        return res.send({success:false,message:"password not match"})        
    const bcri = await bcript.genSalt(10);
    const hashpassword = await bcript.hashSync(password,bcri);
    await foundUser.set({password:hashpassword,forgetPasswordCode:undefined}).save()        
    res.send({passwordChanged:true,verified:true})   
})  
router.post('/forgetPassword',async (req,res) => {
    const email = req.body.email
    const code = RandomCodeGenerator("A0", 7)
    if(!email)
        return res.send({success:false,message:'InvalidEmail'})
    const foundUser = await User.findOne({email:email})
    if(!foundUser)
        return res.send({success:false,message:"UserNotFound"})
    await foundUser.set({forgetPasswordCode:code}).save()        
    const emailOptions  = {
        subject : 'Shopping Cart forget password',
        text : `Please enter this code ${code} to reset your passoword`,
        from : 'ebee.giftshop@gmail.com',
        to : email
    }
    transporter.sendMail(emailOptions, async (err, data) => {
        if (err) {
          res.status(500).send({ success: false, message: err });
        } else {
          res.send({ success: true,code:code, message: "email successfully sent!" });
        }
      });
})  
router.get('/verify/:code',async (req,res)=>{
    const code = req.params.code
    const foundUser =  await User.findOne({forgetPasswordCode:code})
    if(!foundUser)
        return res.send({verified:false})
    res.send({verified:true})
})
router.post('/register',async (req,res) => {

    //check user already exits
    const emailExitst = await User.findOne({email:req.body.email})
    if(emailExitst) return res.status(400).json({message:"email already exits"})
     //password hash
     const bcri = await bcript.genSalt(10);
     const hashpassword = await bcript.hashSync(req.body.password,bcri);
 
     const user = new User({
         uid : req.body.uid,
         name : req.body.name,
         email : req.body.email,
         IsAdmin: req.body.IsAdmin,
         password : hashpassword
     });
      
     try {
        
         const saveduser = await user.save()
         const token = jwt.sign({_id: saveduser._id},process.env.TOKEN_SECRET)
         res.json({Token:token,user:saveduser});
 
     }catch(err) {
         res.status(400).json(err);
     }

 });
 router.post('/login', async (req,res) => {
    const emailExitst = await User.findOne({email:req.body.email})
   if(!emailExitst) return res.json({message:"email or password invalid"});
   //passwordcheck
   const validpass = await bcript.compareSync(req.body.password,emailExitst.password)
   if(!validpass) return res.json({message: "invalid username or password"})
  //token create
    const token = jwt.sign({_id: emailExitst._id},process.env.TOKEN_SECRET)
    return res.json({message: "successfully login",Token:token,IsAdmin:emailExitst.IsAdmin,UserId:emailExitst._id})
});
module.exports = router