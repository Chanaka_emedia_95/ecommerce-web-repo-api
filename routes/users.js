const express = require('express');
const router = express.Router();
const userModel = require('../models/User')
router.get("/:id",async(req,res)=>{
    
    let reqID=req.params.id
    try{
    let user=await userModel.findOne({_id: reqID});
    
    if(!user){
        return res.status(404).json("No such User")
    }
    res.send(user)
    }
    catch(err){
        return res.status(500).send({"error":err.message});
    }
  
});
router.put("/:id",async (req,res)=>{
    let reqID=req.params.id
    try{
    let user= await userModel.findByIdAndUpdate(reqID,req.body)
    if(!user){
        return res.status(404).send("no such User")
    }
    return res.send("User updated successfully");
    }
    catch(err){
        return res.status(500).send({"error":err.message});
    }
});


module.exports = router