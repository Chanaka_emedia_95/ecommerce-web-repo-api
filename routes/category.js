const express = require('express');
const router = express.Router();
const categoryModel = require('../models/category')
router.get("/getall",async (req,res)=>{
    try{
        let categories= await categoryModel.find();
        res.json(categories);
    }catch(ex){
        return res.status(500).send({"error":ex.message});
    }
    
});
module.exports = router