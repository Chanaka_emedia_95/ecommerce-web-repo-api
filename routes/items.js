const express = require('express');
const router = express.Router();
const itemModel = require('../models/item')
router.get("/getall", async (req, res) => {
    try {
        let items = await itemModel.find();
        res.json(items);
    } catch (ex) {
        return res.status(500).json({"error" : ex.message});
    }
});
router.get('/getviewmoreitems', async (req, res) => {
    const id = req.query.id;
    try {
        if (id == "") {
            res.status(404).json({ message: "Id is required" });
        }
        else {
            const item = await itemModel.find({ _id: id });
            res.status(200).json(item)
        }
    } catch (err) {
        res.status(500).json({ message: err })
    }
});
router.get('/getenableitems' ,async (req,res) => {
    const status = req.query.status;
        try{
            const item = await itemModel.find({status:status});
            res.status(200).json(item)
        }catch(err){
            res.status(404).json({message:err})
        }
});

module.exports = router
